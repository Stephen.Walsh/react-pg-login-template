const db = require('../database')

class Users {

    static retrieveAll (callback) {
        db.query('SELECT * FROM users', function (err, res) {
            if(err.error) { return callback(err); }
            callback(res);
        });
    }

    static findUserByUsername (username, callback) {
        return db.query(`SELECT * FROM users WHERE username LIKE '${username}'`, function (err, res) {
            callback(err, res)
        });
    }

    static createUser (username, password, callback) {
        return db.query('INSERT INTO users (username, password) VALUES ($1, $2)', [username, password], function (err, res){
            callback(err)
        });
    }

    static updateUserPassword (username, password, callback) {
        return db.query(`UPDATE users SET password = '${password}' WHERE username LIKE '${username}'`, function (err, res){
            callback(err, res)
        });
    }
}

module.exports = Users;