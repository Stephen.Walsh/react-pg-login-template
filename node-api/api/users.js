const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Users = require('../models/users');
const router = express.Router();

const SECRET_KEY = "secretkey23456";

router.post('/register', (req, res) => {
    const  username = req.body.username;
    const  password = bcrypt.hashSync(req.body.password);

    Users.createUser(username, password, (err)=>{
        if(err.error) return  res.status(500).send("Server error!");
        Users.findUserByUsername(username, (err, user)=>{
            if (err.error) return  res.status(500).send('Server error!');  
            const  expiresIn  =  Date.now() + (24  *  60  *  60);
            const  accessToken  =  jwt.sign({ _id:  user._id }, SECRET_KEY, { expiresIn:  expiresIn });
            res.status(200).send({ "user":  user, "access_token":  accessToken, "expiry":  expiresIn });
        });
    });
});

router.post('/login', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    Users.findUserByUsername(username, (err, user)=>{
        if (err.error) return res.status(500).send('Server error!');  
        if (!user[0]) return res.status(404).send('User not found!');
        const result = bcrypt.compareSync(password, user[0].password);
        if(!result) return res.status(401).send('Password not valid!');

        const  expiresIn  =  Date.now() + (24  *  60  *  60);
        const  accessToken  =  jwt.sign({ _id:  user._id }, SECRET_KEY, { expiresIn: expiresIn });
        res.status(200).send({ "access_token":  accessToken, "expiry":  expiresIn});
    });
});

router.use('/', (req, res, next) => {
    if (req.userContext) {
      return res.redirect('/')
    }
    next()
});

router.post('/users/forgotpassword', (req, res) => {
    const username = req.body.username;
    Users.findUserByUsername(username, (err, user)=>{
        if (err.error) return res.status(500).send('Server error!');  
        if (!user[0]) return res.status(404).send('User not found!');
        res.status(200).send({ "valid_email": true });
    });
});

router.put('/forgotPasswordSubmit', (req, res) => {
    const username = req.body.username;
    const password = bcrypt.hashSync(req.body.password);

    Users.findUserByUsername(username, (err, user)=>{
        if (err.error) return res.status(500).send('Server error!');  
        if (!user[0]) return res.status(404).send('User not found!');
        Users.updateUserPassword (username, password, (err)=>{
            if(err.error) return  res.status(500).send("Server error!");
            res.status(200).send({ "resetPasswordSubmit": true });
        });
    });
});

module.exports = router;