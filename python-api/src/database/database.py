from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine('postgresql://postgres:postgres@react-pg-login-template_postgres_1/users')
Session = sessionmaker(bind=engine)
session = Session()

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

def init_db():
    from user.user_model import User
    from password_token.model import Token

    Base.metadata.create_all(engine)