from sqlalchemy import create_engine, Column, String, Integer
from database.database import Base

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key = True)
    username = Column(String)
    password = Column(String)

    def __repr__(self):
        return "<User(username='%s', password='%s')>" % ( 
            self.username, self.password)

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}