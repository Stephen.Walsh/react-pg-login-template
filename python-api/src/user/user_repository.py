from database.database import session
from .user_model import User

class UserRepository():
    def insert_and_update_user(self, user):
        session.add(user)
        session.commit()
        return user

    def get_user_by_username(self, username):
        user = session.query(User).filter(User.username == username).first()
        return user

    def get_user_by_id(self, id):
        user = session.query(User).filter(User.id == id).first()
        return user

    def get_all_users(self):
        users = session.query(User).all()
        return users
