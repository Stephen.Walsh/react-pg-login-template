from flask import Blueprint, request, Response, make_response
from .service import get_all_users, get_user_by_id, get_user_by_username, update_password
from authentication.jwt.token_required import token_required
import json
import logging

users_blueprint = Blueprint('users', __name__)
   
@users_blueprint.route('/users', methods=['GET'])
@token_required
def get_users():
    username = request.args.get('username')

    if username:
        (user, status_code) = (username)
        return Response(json.dumps(user), status=status_code, mimetype='application.json')

    all_users = get_all_users()
    return Response(json.dumps(all_users), status=200, mimetype='application.json')


@users_blueprint.route('/users/<int:id>', methods=['GET'])
@token_required
def get_by_id(id):
    (user, status_code) = get_user_by_id(id)
    return Response(json.dumps(user), status=status_code, mimetype='application.json')


@users_blueprint.route('/users/password', methods=['PUT'])
def update_user_password():
    if (request.is_json):
        request_data = request.get_data()

        parsed_json = json.loads(request_data)

        status_code = update_password(parsed_json)

        return Response('', status=status_code, mimetype='application.json')

    return Response('', status=200, mimetype='application.json')