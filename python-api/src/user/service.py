from utils.errors import error_response
import logging
from .user_repository import UserRepository
from password_token.repository import TokenRepository
from password_token.service import is_token_valid, invalidate_token
import smtplib, ssl
from random import randint
import bcrypt

user_repository = UserRepository()
token_repository = TokenRepository()

def get_user_by_id(id):
    user = user_repository.get_user_by_id(id)

    if user is None:
        error = error_response('User has not been found')
        return (error, 404)
    
    return (user.as_dict(), 200)


def get_user_by_username(username):
    user = user_repository.get_user_by_username(username)

    if user is None:
        error = error_response('User has not been found')
        return (error, 404)
    
    return (user.as_dict(), 200)

def get_all_users():
    data = user_repository.get_all_users()

    users = list(map(lambda user: user.as_dict(), data))

    return users

def update_password(data):
    tokenObj = token_repository.get_token(data['token'])

    if is_token_valid(tokenObj):
        user = user_repository.get_user_by_id(tokenObj.user_id)

        if user is None:
            return 404

        hashed_password = bcrypt.hashpw(data['password'].encode('utf-8'), bcrypt.gensalt())
        user.password = hashed_password.decode('utf-8')
        user_repository.insert_and_update_user(user)

        invalidate_token(tokenObj)

        return 200

    return 403