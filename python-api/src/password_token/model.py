from sqlalchemy import create_engine, Column, String, Integer, ForeignKey, DateTime, Boolean
from database.database import Base
from sqlalchemy.orm import relationship

class Token(Base):
    __tablename__ = 'token'

    id = Column(Integer, primary_key = True)
    token = Column(String)
    expiry = Column(DateTime)
    user_id = Column(Integer, ForeignKey('users.id', ondelete='CASCADE'), nullable = False)
    kind = Column(String)
    is_valid = Column(Boolean)

    def __repr__(self):
        return "<Token(token='%s', expiry='%s', user_id='%s', kind='%s', is_valid='%s')>" % ( 
            self.token, self.expiry, self.user_id, self.kind, self.is_valid)

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}