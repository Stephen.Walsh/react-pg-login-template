from database.database import session
from .model import Token

class TokenRepository():
    def insert_and_update_token(self, token):
        session.add(token)
        session.commit()
        return token

    def get_token(self, token):
        token = session.query(Token).filter(Token.token == token).first()
        return token