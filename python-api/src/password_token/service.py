import uuid
from .model import Token
from .repository import TokenRepository
from user.user_repository import UserRepository
import datetime

token_repository = TokenRepository()
user_repository = UserRepository()

def create_password_reset_token_for_username(username):
    user = user_repository.get_user_by_username(username)

    if user is not None:
        token = uuid.uuid4()
        expiry = datetime.datetime.now() + datetime.timedelta(hours=1)
        
        token_obj = Token(token=token, expiry=expiry, user_id=user.id, kind='password-reset', is_valid=True )

        token_repository.insert_and_update_token(token_obj)

        print('password reset token generated. Please visit http://localhost:5000/password/token/%s' % token)


def send_correct_redirect_link(token):
    tokenObj = token_repository.get_token(token)

    if tokenObj is not None:
        if is_token_valid(tokenObj):
            return 'http://localhost:3001/resetPassword?token=%s' % token


    return 'http://localhost:3001/register'

def is_token_valid(token_obj):
    current_datetime = datetime.datetime.now()
    if current_datetime < token_obj.expiry and token_obj.is_valid == True:
        return True

    return False

def invalidate_token(token_obj):
    token_obj.is_valid = False
    token_repository.insert_and_update_token(token_obj)
