from flask import Flask
from user.routes import users_blueprint
from register.routes import register_blueprint
from password.routes import password_blueprint
from login.routes import login_blueprint
from database.database import init_db

application = Flask(__name__)

def main():
    init_db()
    application.register_blueprint(users_blueprint)
    application.register_blueprint(register_blueprint)
    application.register_blueprint(login_blueprint)
    application.register_blueprint(password_blueprint)
    application.run(host="0.0.0.0")

if __name__ == '__main__':
    main()