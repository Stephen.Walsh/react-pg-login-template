from flask import Blueprint, request, Response, make_response
from .service import process_user_registration
import json

register_blueprint = Blueprint('register', __name__)

@register_blueprint.route('/register', methods=['POST'])
def post_users():
    if (request.is_json):
        request_data = request.get_data()
        
        parsed_json = json.loads(request_data)
        
        (response_data, status_code) = process_user_registration(parsed_json)
        
        return Response(json.dumps(response_data), status=status_code, mimetype='application.json')

    error_response = {
        'message': 'something has gone wrong with this transaction'
    }

    return Response(json.dumps(error_response), status=500, mimetype='application.json')