from utils.errors import error_response
import logging
from user.user_model import User
from user.user_repository import UserRepository
import bcrypt

user_repository = UserRepository()

def process_user_registration(data):
    user = user_repository.get_user_by_username(data['username'])

    if user is None:
        hashed_password = bcrypt.hashpw(data['password'].encode('utf-8'), bcrypt.gensalt())
        user = user_repository.insert_and_update_user(User(username=data['username'], password=hashed_password.decode('utf-8')))

        response_object = {
            'id': user.id
        }

        return (response_object, 201)
    
    error = error_response("An account is already registered to this user")

    return (error, 400)
