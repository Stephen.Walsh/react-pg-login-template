from flask import Blueprint, request, Response, make_response, redirect
import json
from password_token.service import create_password_reset_token_for_username, send_correct_redirect_link

password_blueprint = Blueprint('password', __name__)

@password_blueprint.route('/password/token/reset', methods=['POST'])
def password_token_reset(): 
    # create database for storing token
    # generate token with expiry for username
    if (request.is_json):

        request_data = request.get_data()
        parsed_json = json.loads(request_data)
    
        create_password_reset_token_for_username(parsed_json['username'])

    return Response('', status=202)


@password_blueprint.route('/password/token/<string:token>', methods=['GET'])
def token_redirect(token):
    print(token)

    link = send_correct_redirect_link(token)

    return redirect(link, code=303)
