import jwt
import datetime

key = 'secretKeyData'

def generate_jwt(account_id): 
    jwt_payload = {
        'exp': datetime.datetime.now() + datetime.timedelta(seconds=60),
        'id': account_id,
        'iss': 'login.neueda.io', 
        'iat': datetime.datetime.now(),
        'account': account_id
    }

    encoded = jwt.encode(jwt_payload, key, algorithm='HS256')

    return encoded.decode("utf-8")