import json
import jwt
import time
import sys
from functools import wraps
from flask import request, Response
key = 'secretKeyData'

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']
        if not token:
            return Response(json.dumps({'message' : 'Token is missing!'}), status=401, mimetype='application.json')
        
        # if(is_jwt_valid(token) == False):
        #     return Response(json.dumps({'message' : 'Token is invalid!'}), status=401, mimetype='application.json')
        return f(*args, **kwargs)

    return decorated

def is_jwt_valid(token):
    try:
        jwt.decode(token, key, algorithms='HS256')
        return True
    except jwt.ExpiredSignatureError:
        return False
    except jwt.InvalidIssuerError:
        return False
    return False