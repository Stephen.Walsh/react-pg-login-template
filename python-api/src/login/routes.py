from flask import Blueprint, request, Response, make_response
from .service import login_user as login_service_login_user
import json

login_blueprint = Blueprint('login', __name__)

@login_blueprint.route('/login', methods=['POST'])
def login_user():
    if (request.is_json):
        request_data = request.get_data()
        
        parsed_json = json.loads(request_data)
        
        (response_data, status_code) = login_service_login_user(parsed_json)
        
        return Response(json.dumps(response_data), status=status_code, mimetype='application.json')

    error_response = {
        'message': 'something has gone wrong with this transaction'
    }

    return Response(json.dumps(error_response), status=500, mimetype='application.json')