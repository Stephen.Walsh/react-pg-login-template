from user.user_repository import UserRepository
from utils.errors import error_response
from authentication.jwt.generation import generate_jwt
import datetime
import bcrypt

user_repository = UserRepository()

def login_user(data):
    user = user_repository.get_user_by_username(data['username'])

    if user is None:
        error = error_response("An account with these details does not exist")
        return (error, 404)
    
    if bcrypt.checkpw(data['password'].encode('utf-8'), user.password.encode('utf-8')):
        token = generate_jwt(user.id)
        expiry = datetime.datetime.now() + datetime.timedelta(seconds=30)
        auth = {
            'access_token': token,
            'expiry': expiry.strftime('%s')
        }

        return (auth, 200)
    
    error = error_response("Invalid credentials")
    return (error, 401)