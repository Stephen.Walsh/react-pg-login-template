import { handleResponse } from '../_helpers';
import { authHeader } from '../_helpers';

export const userService = {
    getUsers,
};

function getUsers() {
    return fetch(`http://localhost:5000/users`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': authHeader()
          }
    })
    .then(handleResponse)
    .then(users => {
        return users;
    });
}