import { BehaviorSubject } from 'rxjs';
import { handleResponse } from '../_helpers';

const currentUserSubject = new BehaviorSubject(JSON.parse(extractCookies(document.cookie)));

export const authenticationService = {
    register,
    login,
    logout,
    forgotPassword,
    forgotPasswordSubmit,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue () { return currentUserSubject.value }
};

function login(email, password) {
  return fetch(`http://localhost:5000/login`, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({'username' : email, 'password' : password})
    })
  .then(handleResponse)
  .then(user => {
      document.cookie = `access_token=${user["access_token"]};expiry=${user["expiry"]}`
      const jsonCookie = JSON.parse(extractCookies(document.cookie));
      currentUserSubject.next(jsonCookie);
      return extractCookies(jsonCookie);
  });
}

function forgotPassword(email) {
  return fetch(`http://localhost:5000/password/token/reset`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({'username' : email})
    })
  .then(res => res);
}

function forgotPasswordSubmit(token, password) {
  return fetch(`http://localhost:5000/users/password`, {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({ 
        token, 
        password
      })
    })
  .then(res => res);
}

function register(email, password) {
    fetch('http://localhost:5000/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({'username' : email, 'password' : password})
    })
    .then(res => res.json());
}

function logout() {
    document.cookie = '';
    currentUserSubject.next(null);
}

function extractCookies(cookieStr) {
  var output = {};
  cookieStr.split(/\s*;\s*/).forEach(function(pair) {
    pair = pair.split(/\s*=\s*/);
    output[pair[0]] = pair.splice(1).join('=');
  });
  var json = JSON.stringify(output, null, 4);
  return json;
}