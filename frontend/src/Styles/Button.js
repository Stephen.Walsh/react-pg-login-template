import styled from "./node_modules/styled-components";

export default styled.button`
  background-color: ${props => (props.primary ? "#3bd788" : "#008CBA")};
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  border-radius: 8px;
`;