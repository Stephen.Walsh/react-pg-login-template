import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { PrivateRoute } from '../_components/PrivateRoute';
import Login from '../_components/Login/Login';
import Register from '../_components/Login/Register';
import ForgottenPassword from '../_components/Login/ForgottenPassword/ForgottenPassword';
import Header from '../_components/Header/Header';
import Home from '../_components/Home/Home';
import ResetPassword from '../_components/Login/ForgottenPassword/ResetPassword';

class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Router>
          <PrivateRoute exact path="/" component={Home} />
          <Route path='/login' component={Login}/>
          <Route path='/register' component={Register}/>
          <Route path='/forgottenPassword' component={ForgottenPassword}/>
          <Route path='/resetPassword' component={ResetPassword}/>
        </Router>
    </div>
    );
  }
}

export default App;