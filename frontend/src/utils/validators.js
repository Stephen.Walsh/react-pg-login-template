import { isEmail, isEmpty, isLength, isContainWhiteSpace } from '../_components/Shared/validator';

const noErrorObj = {
    error: false,
};

const errorObj = (errorMessage) => {
    return {
        error: true,
        message: errorMessage,
    };
}

const baseErrorCalcuation = (errorMethods, field, fieldName) => {
    return errorMethods.reduce ((result, errorMethod) => {
        const possibleError = errorMethod(field, fieldName);

        if (possibleError.error) {
            result.push(possibleError.message);
        }

        return result;
    }, []);
}

const passwordValidation = (password) => {
    const errorMethods = [
        fieldIsEmpty, 
        fieldContainsWhiteSpace, 
        containsOneLowerCaseLetter,
        containsOneUpperCaseLetter,
        containsOneNumericCharacter,
        containsOneSpecialCharacter,
    ];

    let errorList = baseErrorCalcuation(errorMethods, password, 'Password');

    if (!isLength(password, { gte: 6, lte: 16, trim: true })) {
        errorList.push("Password's length must between 6 to 16 characters long");
    }

    return errorList;
};

const basicPasswordValidation = (password) => {
    const errorMethods = [
        fieldIsEmpty,
    ];
    
    return baseErrorCalcuation(errorMethods, password, 'Password')
};

const matchingPasswordValidation = (password, matchingPassword) => {
    let errorList = [];

    if (password !== matchingPassword) {
        errorList.push("Passwords must match");
    }

    return errorList;
};

const emailValidation = (email) => {
    let errorMethods = [
        fieldIsEmpty,
        fieldIsEmail,
    ];

    return baseErrorCalcuation(errorMethods, email, 'Email')
};

const containsOneLowerCaseLetter = (password, fieldName) => {
    const regex = new RegExp('(?=.*[a-z])')
    
    if (password.match(regex)) {
        return noErrorObj;
    }

    return errorObj(`${fieldName} should contain at least 1 lowercase chracter`);
};

const containsOneUpperCaseLetter = (password, fieldName) => {
    const regex = new RegExp('(?=.*[A-Z])')
    
    if (password.match(regex)) {
        return noErrorObj;
    }

    return errorObj(`${fieldName} should contain at least 1 uppercase character`);
};

const containsOneNumericCharacter = (password, fieldName) => {
    const regex = new RegExp('(?=.*[0-9])')
    
    if (password.match(regex)) {
        return noErrorObj;
    }

    return errorObj(`${fieldName} should contain at least 1 numeric character`);
};

const containsOneSpecialCharacter = (password, fieldName) => {
    // eslint-disable-next-line
    const regex = new RegExp('(?=.[!@#\$%\^&])')
    
    if (password.match(regex)) {
        return noErrorObj;
    }

    return errorObj(`${fieldName} should contain at least 1 special character`);
};

const fieldIsEmpty = (field, fieldName) => {
    if (isEmpty(field)) {
        return errorObj(`${fieldName} can't be blank`);
    }
    return noErrorObj;
};

const fieldContainsWhiteSpace = (field, fieldName) => {
    if (isContainWhiteSpace(field)) {
        return errorObj(`${fieldName} should not contain white spaces`);
    }
    
    return noErrorObj;
};

const fieldIsEmail = (field, _) => {
    if (isEmail(field)) {
        return noErrorObj;
    }

    return errorObj(`Please enter a valid email`)
};

export {
    passwordValidation,
    matchingPasswordValidation,
    emailValidation,
    basicPasswordValidation,
};