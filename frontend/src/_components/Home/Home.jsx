import React, { useState, useEffect  } from 'react';
import { Link } from 'react-router-dom';
import { authenticationService, userService } from '../../_services';


const Home = props => {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            console.log(`Logged in as: ${authenticationService.currentUserValue.access_token}`);

            const result = await userService.getUsers();
            setUsers(result);
        };
        fetchData();
    }, []);

    return (
        <div style={{'margin': '0 auto', 'maxWidth': '900px', 'padding': '40px 40px'}}>

        <h4>Hi, You're logged in with React</h4>

        <p>The table below uses a authenticated request:</p>

        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody>
                {users.map(item => (
                    <tr key={item.id}>
                        <th>{item.id}</th>
                        <th>{item.username}</th>
                        <th>{item.password}</th>
                    </tr>
                ))}
            </tbody>

        </table>

        <p> <Link to="/login">Logout</Link> </p>
        </div>
    );
}

export default Home;