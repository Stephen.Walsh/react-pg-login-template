import React, { useState } from "react";
import { FormGroup, Button } from "react-bootstrap";
import Email from '../../Input/Email';
import { authenticationService } from '../../../_services';
import { emailValidation } from '../../../utils/validators';
import "./ResetPassword.css";

const ForgottenPassword = (props) => {

  const [email, setEmailData] = useState('');
  const [emailErrors, setEmailErrors] = useState([]);
  const emailLabel = 'Email';

  const [emailSent, setEmailSent] = useState(false);

    const handleForgottenPasswordClick = async event => {
        event.preventDefault();
        setEmailSent(true);
        try {
            await authenticationService.forgotPassword(email);
        } catch (e) {
            // We choose not to display error in frontend
        }
    };

    const emailCallback = (event) => {
      const email = event.target.value;
      processEmail(email);
    };

    const processEmail = (email) => {
      setEmailData(email);

      const errorList = emailValidation(email);
      setEmailErrors(errorList);
  };

  const renderRequestCodeForm = () => {
    return (
        <form onSubmit={handleForgottenPasswordClick}>
        <FormGroup bsSize="large" controlId="email">
            <Email parentCallback={emailCallback} errors={emailErrors} placeholder='Enter your email' label={emailLabel}/>
        </FormGroup>
        <Button type="submit" bsStyle="primary">Request New Password</Button>
        </form>
    );
  };

  const renderSuccessMessage = () => {
    return (
      <div className="success">
        <p>Password Request has been sent.</p>
        <p>Please check your email and follow the instructions.</p>
      </div>
    );
  };
    
    return (
      <div className="ResetPassword">
      {!emailSent
        ? renderRequestCodeForm()
        : renderSuccessMessage()}
      </div>
    );
}
export default ForgottenPassword;