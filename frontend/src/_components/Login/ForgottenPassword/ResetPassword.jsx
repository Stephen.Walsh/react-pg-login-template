import React, { useState } from "react";
import { Button } from 'react-bootstrap';
import Password from '../../Input/Password';
import { authenticationService } from '../../../_services';
import { passwordValidation, matchingPasswordValidation } from '../../../utils/validators';

const ResetPassword = (props) => {

    const [password, setPasswordData] = useState('');
    const [passwordErrors, setPasswordErrors] = useState([]);
    const [matchingPassword, setMatchingPassword] = useState('');
    const [matchingPasswordErrors, setMatchingPasswordErrors] = useState('');
    let resetTokenParam = `token`;

    const getResetTokenParam = () => {
      let params = new URLSearchParams(window.location.search);
      return params.get(resetTokenParam);
    };

    const passwordCallback = (event) => {
      const password = event.target.value;
      processPassword(password)
    }

    const processPassword = (password) => {
        setPasswordData(password);

        const errorList = passwordValidation(password);
        setPasswordErrors(errorList);

        if (matchingPassword ) {
            const matchingErrorList = matchingPasswordValidation(password, matchingPassword);
            setMatchingPasswordErrors(matchingErrorList);
        }
    };

    const matchingPasswordCallback = (event) => {
      const matchingPassword = event.target.value;
      processMatchingPassword(matchingPassword);
    };

    const processMatchingPassword = (matchingPassword) => {
        setMatchingPassword(matchingPassword);
        
        const errorList = matchingPasswordValidation(password, matchingPassword);
        setMatchingPasswordErrors(errorList);
    };
  
    const validateRegisterForm = () => {
      const totalErrors = passwordErrors.length + matchingPasswordErrors.length

      if (totalErrors === 0 && password.length > 0 && matchingPassword.length > 0) {
          return true;
      }

      processPassword(password);
      processMatchingPassword(matchingPassword);
      
      return false;
    }

    const resetPassword = async event => {
        event.preventDefault();
        const isFormValid = validateRegisterForm();

        if(isFormValid === true) {
          try {
            //TODO: update server APIs to consider relationship table 
            const token = getResetTokenParam()
            await authenticationService.forgotPasswordSubmit(token, password);

            alert(`Password has now been reset.`);
            props.history.push('/login');
            } catch (event) {
              alert(event.message);
            }
        }
    }

    return (
      <div style={{'margin': '0 auto', 'maxWidth': '400px', 'border': '1px solid #d8dee2', 'padding': '40px 40px'}}>
      <h2 style={{'textAlign': 'center'}}>Reset Password</h2>
      <br/>
      <form onSubmit={resetPassword}>
          <Password parentCallback={passwordCallback} errors={passwordErrors} placeholder='New Password' label='New Password'/>
          <Password parentCallback={matchingPasswordCallback} errors={matchingPasswordErrors} placeholder='Confirm password' label='Confirm password'/>
          <Button type="submit" bsStyle="primary">Reset Password</Button>
      </form>
  </div>
    );
};
export default ResetPassword;