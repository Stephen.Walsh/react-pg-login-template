import React, { useState } from "react";
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import { authenticationService } from '../../_services';
import Password  from '../Input/Password';
import Email from '../Input/Email';
import { basicPasswordValidation, emailValidation } from '../../utils/validators';

const Login = props => {
    const [error, setError] = useState('');

    const [email, setEmailData] = useState('');
    const [emailErrors, setEmailErrors] = useState([]);
    const [password, setPasswordData] = useState('');
    const [passwordErrors, setPasswordErrors] = useState([]);

    const emailLabel = 'Email';
    const passwordLabel = 'Password';

    const emailCallback = (event) => {
        const email = event.target.value;
        processEmail(email);
    };

    const processEmail = (email) => {
        setEmailData(email);

        const errorList = emailValidation(email);
        setEmailErrors(errorList);
    };

    const passwordCallback = (event) => {
        const password = event.target.value;
        processPassword(password);
    };

    const processPassword = (password) => {
        setPasswordData(password);

        const errorList = basicPasswordValidation(password);
        setPasswordErrors(errorList);
    };

    const validateLoginForm = () => {
        const totalErrors = emailErrors.length + passwordErrors.length;

        if (totalErrors === 0 && email.length > 0 && password.length > 0) {
            return true;
        } 

        processEmail(email);
        processPassword(password);

        return false;
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const isFormValid = validateLoginForm();
        
        if(isFormValid === true) {
            authenticationService.login(email, password)
                .then(
                    user => {
                        const { from } = props.location.state || { from: { pathname: "/" } };
                        props.history.push(from);
                    },
                    error => setError('Incorrect username or password.')
                );
        } 
    }

    let content = (
        <div style={{'margin': '0 auto', 'maxWidth': '400px', 'border': '1px solid #d8dee2', 'padding': '40px 40px'}}>
            <h2 style={{'textAlign': 'center'}}>Sign In</h2>
            <br/>
            {error && <div className={'alert alert-danger'}>{error}</div> }
            <form onSubmit={handleSubmit}>
                <Email parentCallback={emailCallback} errors={emailErrors} placeholder='Enter your email' label={emailLabel}/>
                <Link to="/forgottenPassword" style={{'float': 'right'}}>Forgotten Password?</Link> 
                <Password parentCallback={passwordCallback} errors={passwordErrors} placeholder='Enter your password' label={passwordLabel}/>
                <Button type="submit" bsStyle="primary">Login</Button>
            </form>
        </div>

    );
    return content;
};


export default Login;