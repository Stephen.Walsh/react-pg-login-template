import React, { useState } from "react";
import { Button, } from 'react-bootstrap';
import { authenticationService } from '../../_services';
import Password  from '../Input/Password';
import Email from '../Input/Email';
import { passwordValidation, matchingPasswordValidation, emailValidation } from '../../utils/validators';

const Register = props => {

    const [email, setEmailData] = useState('');
    const [emailErrors, setEmailErrors] = useState([]);
    const [password, setPasswordData] = useState('');
    const [passwordErrors, setPasswordErrors] = useState([]);
    const [matchingPassword, setMatchingPassword] = useState('');
    const [matchingPasswordErrors, setMatchingPasswordErrors] = useState('');

    const passwordCallback = (event) => {
        const password = event.target.value;
        processPassword(password)
    }

    const processPassword = (password) => {
        setPasswordData(password);

        const errorList = passwordValidation(password);
        setPasswordErrors(errorList);

        if (matchingPassword ) {
            const matchingErrorList = matchingPasswordValidation(password, matchingPassword);
            setMatchingPasswordErrors(matchingErrorList);
        }
    };

    const emailCallback = (event) => {
        const email = event.target.value;
        processEmail(email)
    };

    const processEmail = (email) => {
        setEmailData(email);

        const errorList = emailValidation(email);
        setEmailErrors(errorList);
    };

    const matchingPasswordCallback = (event) => {
        const matchingPassword = event.target.value;
        processMatchingPassword(matchingPassword);
    };

    const processMatchingPassword = (matchingPassword) => {
        setMatchingPassword(matchingPassword);
        
        const errorList = matchingPasswordValidation(password, matchingPassword);
        setMatchingPasswordErrors(errorList);
    };

    const validateRegisterForm = () => {

        const totalErrors = emailErrors.length + passwordErrors.length + matchingPasswordErrors.length

        if (totalErrors === 0 && email.length > 0 && password.length > 0 && matchingPassword.length > 0) {
            return true;
        }

        processEmail(email);
        processPassword(password);
        processMatchingPassword(matchingPassword);
        
        return false;
    }

    const register = e => {
        e.preventDefault();
        const isFormValid = validateRegisterForm();

        if(isFormValid === true){
            authenticationService.register(email, password);
            alert(`${email} is now registered`);
            props.history.push('/login');
        }
    }

    let content = (
        <div style={{'margin': '0 auto', 'maxWidth': '400px', 'border': '1px solid #d8dee2', 'padding': '40px 40px'}}>
            <h2 style={{'textAlign': 'center'}}>Sign Up</h2>
            <br/>

            <form onSubmit={register}>
                <Email parentCallback={emailCallback} errors={emailErrors} placeholder='Enter your email' label='Email'/>
                <Password parentCallback={passwordCallback} errors={passwordErrors} placeholder='Enter your password' label='Password'/>
                <Password parentCallback={matchingPasswordCallback} errors={matchingPasswordErrors} placeholder='Confirm password' label='Confirm password'/>
                <Button type="submit" bsStyle="primary">Register</Button>
            </form>
        </div>
    );
    return content;
};

export default Register;