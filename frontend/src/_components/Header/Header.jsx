import React from 'react';
import './header.css'
import neuedaIcon from '../../resources/images/neueda.png'

function Header() {
    return (
        <div className="header">
            <div className="company-image-container">
                <img src={neuedaIcon} alt="logo"/>
            </div>
            <div className="menu">
                <ul id="nav">
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Register</a></li>
                </ul>
            </div>
        </div>
    )
}

export default Header;