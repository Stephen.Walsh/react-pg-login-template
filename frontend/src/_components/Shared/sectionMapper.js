import React, { useReducer } from 'react';

const SectionMapper = (props) => {
    var initialState = { };

    const reducer = (state, action) => {
        state[action.fieldName] = action.input;
        props.parentCallback(state);
        return state;
    };

    const [data, dispatch] = useReducer(reducer, initialState);

    const onInputChange = (fieldName) => (input) => {
        dispatch({fieldName, input});
    };

    return (
        <div> 
            {
                props.sections.map((section, index) => 
                    <input 
                        name={section.name} 
                        placeholder={section.placeholder} 
                        type={section.type} 
                        key={index}
                        onInputChange={onInputChange(section.name)}
                    />
                ) 
            }
        </div>
    );
};

export default SectionMapper;