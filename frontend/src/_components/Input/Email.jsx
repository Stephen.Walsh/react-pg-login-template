import React from "react";
import { FormGroup, FormControl, ControlLabel, HelpBlock } from 'react-bootstrap';

const Email = props => {
    return (
        <div className="email">
            <FormGroup controlId={props.label}>
                <ControlLabel>{props.label}</ControlLabel>
                <FormControl type="text" name={props.label} placeholder={props.placeholder} onChange={props.parentCallback} />
                { props.errors && props.errors.map( (error, index) => 
                    <div key={index}>
                        <HelpBlock className={'alert-danger'}>{error}</HelpBlock>
                    </div>
                        
                    )
                }
            </FormGroup>
        </div>
    );
};

export default Email;