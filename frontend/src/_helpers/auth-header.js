import { authenticationService } from '../_services';

export function authHeader() {
    const currentUser = authenticationService.currentUserValue;
    if (currentUser && currentUser.token) {
        return `${currentUser.token}`;
    } else {
        return {};
    }
}